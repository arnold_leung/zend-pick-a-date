<?php

class Application_Form_Registration extends Zend_Form
{
       public function init()
    {
		
		$this->setName("Registration");
		
		// Set the method for the display form to POST
		$this->setMethod('post');
		
        $firstname = $this->createElement('text','first_name');
        $firstname->setLabel('First Name:')
					->addValidator('StringLength',false,array (3))
                    ->setRequired(false);
                    
        $lastname = $this->createElement('text','last_name');
        $lastname->setLabel('Last Name:')
					->addValidator('StringLength',false,array (3))
                    ->setRequired(false);
                    
        $email = $this->createElement('text','email');
        $email->setLabel('Email: *')
				->addValidator(new Zend_Validate_EmailAddress())
                ->setRequired(true);
                
        $username = $this->createElement('text','member_login');
        $username->setLabel('User Name/ member login: *')
				->addValidator('StringLength',false,array (5))
                ->setRequired(true);
                
        $password = $this->createElement('password','member_password');
        $password->setLabel('Password: *')
				->addValidator('StringLength',false,array (5))
                ->setRequired(true);
                
        $confirmPassword = $this->createElement('password','confirmPassword');
        $confirmPassword->setLabel('Confirm Password: *')
                ->setRequired(true);
                
        $register = $this->createElement('submit','register');
        $register->setLabel('Sign up')
                ->setIgnore(true);
                
        $this->addElements(array(
                        $firstname,
                        $lastname,
                        $email,
                        $username,
                        $password,
                        $confirmPassword,
                        $register
        ));
    }
    


}
