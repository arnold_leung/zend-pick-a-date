<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
		
        $this->setName("login");
		
		// Set the method for the display form to POST
		$this->setMethod('post');
	
	 // Add an username(email) element
	
	$this->addElement('text', 'username', array(
					'filters' => array('StringTrim', 'StringToLower'),
					'validators' => array(
					array('StringLength', false, array(0, 50)),
					),
					'required' => true,
					'label' => 'Username (email):',
					));

	 // Add an password element
	$this->addElement('password', 'password', array(
					'filters' => array('StringTrim'),
					'validators' => array(
					array('StringLength', false, array(0, 50)),
					),
					'required' => true,
					'label' => 'Password:',
					));

	 // Add an submit
	$this->addElement('submit', 'login', array(
					'required' => false,
					'ignore' => true,
					'label' => 'Login',
					));
    }


}

