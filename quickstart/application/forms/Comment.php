<?php

class Application_Form_Comment extends Zend_Form
{

    public function init()
    {
         // Set the method for the display form to POST
        $this->setMethod('post');
		
		// Add the comment element
        $this->addElement('textarea', 'comment', array(
            'label'      => 'your Comment here:',
            'required'   => true,
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
                )
        ));
		
		
		
		 // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'submit comment',
        ));
    }


}

