<?php

class Application_Form_Editprofile extends Zend_Form
{

    public function init()
    {
        $firstname = $this->createElement('text','first_name');
        $firstname->setLabel('First Name:')
                    ->setRequired(false);
                    
        $lastname = $this->createElement('text','last_name');
        $lastname->setLabel('Last Name:')
                    ->setRequired(false);
                    
        $email = $this->createElement('text','email');
        $email->setLabel('Email: *')
                ->setRequired(true);
				
        $birthday = $this->createElement('text','birthday');
        $birthday->setAttrib('id', 'birthday')
				->setLabel('Birthday:')
                ->setRequired(true);  
                
        $password = $this->createElement('password','member_password');
        $password->setLabel('New Password: *')
                ->setRequired(true);
                
        $confirmPassword = $this->createElement('password','confirmPassword');
        $confirmPassword->setLabel('Confirm New Password: *')
                ->setRequired(true);
                
        $update = $this->createElement('submit','update');
        $update->setLabel('Update')
                ->setIgnore(true);
                
        $this->addElements(array(
                        $firstname,
                        $lastname,
                        $email,
						$birthday,
                        $password,
                        $confirmPassword,
                        $update
        ));
    }


}

