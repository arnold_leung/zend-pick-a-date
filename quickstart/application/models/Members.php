<?php

class Application_Model_Members extends Zend_Db_Table_Abstract

{
protected $_name='members';
protected $_primary='member_login';
//check unique id
function checkUnique($member_login)
    {
        $select = $this->_db->select()
                            ->from($this->_name,array('member_login'))
                            ->where('member_login=?',$member_login);
        $rows = $this->_db->fetchAll($select);
		//$result = $this->getAdapter()->fetchOne($select);
        if($rows){
            return true;
        }
        return false;
    }
}

