<?php
class Application_Model_Plans extends Zend_Db_Table_Abstract
{
    protected $_venue;
    protected $_gift;
    protected $_activity;
	protected $_note;
    protected $_id;
	protected $_author;
	protected $_name;
	
    public function __construct(array $options = null)
    {
  
		if (is_array($options)) {

		$this->setOptions($options);
		
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Plan property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Plan property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
				
        $methods = get_class_methods($this);
		
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
			
            if (in_array($method, $methods)) {

				$this->$method($value);
            }
		
        }
		
        return $this;
		
    }
	
 // Name get and set
    public function setAuthor($name)
    {
        $this->_author = (string) $name;
		
        return $this;
    }
 
    public function getAuthor()
    {
        return $this->_author;
    }
	
	 // Name get and set
    public function setName($name)
    {
        $this->_name = (string) $name;
		
        return $this;
    }
 
    public function getname()
    {
        return $this->_name;
    }
	
 // Veune get and set
    public function setVenue($venue)
    {
        $this->_venue = (string) $venue;
		
        return $this;
    }
 
    public function getvenue()
    {
        return $this->_venue;
    }
 // gift get and set
	public function setGift($gift)
    {
        $this->_gift = (string) $gift;
        return $this;
    }
 
    public function getgift()
    {
        return $this->_gift;
    }
 // Activity get and set
    public function setActivity($activity)
    {
        $this->_activity = (string) $activity;
        return $this;
    }
     public function getactivity()
    {
        return $this->_activity;
    }
 // note get and set
    public function getnote()
    {
        return $this->_note;
    }
    public function setNote($note)
    {
        $this->_note = $note;
        return $this;
    }
 // id get and set
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}

?>