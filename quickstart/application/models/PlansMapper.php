<?php

class Application_Model_PlansMapper
{
 protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Plan');
        }
        return $this->_dbTable;
    }
 
    public function save(Application_Model_Plans $plan)
    {
        $data = array(
			
            'venue'   => $plan->getvenue(),
            'gift' => $plan->getgift(),
            'activity' => $plan->getactivity(),
			'note' => $plan->getnote(),
			'author'=>$plan->getAuthor(),
			'name'=>$plan->getName(),
        );
 
        if (null === ($id = $plan->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
 
    public function find($id, Application_Model_Plans $plan)
    {
        $result = $this->getDbTable()->find($id);
		
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();

        $plan->setId($row->id)
                  ->setVenue($row->venue)
                  ->setactivity($row->activity)
                  ->setgift($row->gift)
				  ->setnote($row->note)
				  ->setName($row->name)
				  ->setAuthor($row->author);
		
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Plans();
            $entry->setId($row->id)
                  ->setVenue($row->venue)
                  ->setactivity($row->activity)
                  ->setgift($row->gift)
				  ->setnote($row->note)
				  ->setName($row->name)
				  ->setAuthor($row->author);
            $entries[] = $entry;
        }
        return $entries;
    }

}

