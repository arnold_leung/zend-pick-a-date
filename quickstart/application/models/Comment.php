<?php

class Application_Model_Comment
{
	protected $_id;
	protected $_plan_id;
	protected $_author;
	protected $_comment;
	protected $_time_created;
	
	
	public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
	
	 public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid  comment property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid comment property');
        }
        return $this->$method();
    }
	
	
	    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
	
	//set and get for id
	public function setId($Id)
    {
        $this->_id = (int) $Id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
	//set and get for plan id
	public function setPlan_id($plan_id)
    {
        $this->_plan_id = (int) $plan_id;
        return $this;
    }
 
    public function getPlan_id()
    {
        return $this->_plan_id;
    }
	//set and get for author
	public function setAuthor($Author)
    {
        $this->_author = (string) $Author;
        return $this;
    }
 
    public function getAuthor()
    {
        return $this->_author;
    }
	
	//set and get for author
	public function setComment($comment)
    {
        $this->_comment = (string) $comment;
        return $this;
    }
 
    public function getComment()
    {
        return $this->_comment;
    }
	//set and get for author
	public function setTime_created($ts)
    {
        $this->_time_created = $ts;
        return $this;
    }
 
    public function getTime_created()
    {
        return $this->_time_created;
    }
	
	
}

