<?php

class Application_Model_CommentMapper
{
protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Comment');
        }
        return $this->_dbTable;
    }
	
	public function save(Application_Model_Comment $comment)
    {
		

		$storage = new Zend_Auth_Storage_Session();
        $SessionData = $storage->read();
		
/*		if (isset($SessionData)){
			echo $SessionData->member_login;
		}
*/
		
        $data = array(
            'plan_id'   => $comment->getPlan_id(),
			'author'   => $SessionData->member_login,
            'comment' => $comment->getComment(),
            'time_created' => date('Y-m-d H:i:s'),
        );
 
        if (null === ($id = $comment->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
	
	public function fetchByPlanId($PlanId)
    {
		$db = $this->getDbTable();
		$select = $db->select()->where('plan_id = ?', $PlanId);
		$resultSet = $db->fetchAll($select);
		$entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Comment();
            $entry->setId($row->id)
					->setPlan_Id($row->plan_id)
                  ->setAuthor($row->author)
                  ->setComment($row->comment)
                  ->setTime_created($row->time_created);
            $entries[] = $entry;
        }
        return $entries;
		
	}
	
	public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Comment();
            $entry->setId($row->id)
					->setPlan_Id($row->plan_id)
                  ->setAuthor($row->author)
                  ->setComment($row->comment)
                  ->setTime_created($row->time_created);
            $entries[] = $entry;
        }
        return $entries;
    }
	
}

