<?php

class MembersController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
		$storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
		
		//if login then redirect to homepage 
        if($data){
			$username = $data->member_login;
			$this->view->username = $username;
        }else{
			$this->_redirect('/members/login');
		}
		
    }

    public function loginAction()
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
		
		//if login then redirect to homepage 
        if($data){
			$this->view->errorMessage = "You are logged in, please log out before proceeding";
            $this->_redirect('/members');
			return;
        }
		
		//connect to db
		$db = new Zend_Db_Adapter_Pdo_Mysql(array(
			'host'     => 'localhost',
			'username' => 'root',
			'password' => '',
			'dbname'   => 'test'
			));
		//setup form
		$form = new Application_Form_Login();
		$this->view->form = $form;
	
		//handle request
	   if($this->getRequest()->isPost()){
            if($form->isValid($_POST)){
                
                //setup auth adaptor
                $adapter = new Zend_Auth_Adapter_DbTable(
                $db,
                'members',
                'member_login',
                'member_password');
				//setup authenticate
                $adapter->setIdentity($form->getValue('username'));
				$adapter->setCredential($form->getValue('password'));
				
				$auth   = Zend_Auth::getInstance();
				$result = $auth->authenticate($adapter);
				
                if($result->isValid()){
                    $storage = new Zend_Auth_Storage_Session();
					$storage->write($adapter->getResultRowObject());
                    $this->_redirect('/');
					
                } else {
                    $this->view->errorMessage = "Invalid username or password. Please try again.";
                }         
            }
        }
	
    }

    public function logoutAction()
    {
        // action body
		$storage = new Zend_Auth_Storage_Session();
        $storage->clear();
    }

    public function registerAction()
    {
        //check if user has logged in
		$storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if($data){
			$this->view->errorMessage = "You are logged in, please log out before proceeding";
            $this->_redirect('/members');
			return;
        }
		
		//create form object
		
		$form = new Application_Form_Registration();
        
		//pass form object to view
		$this->view->form=$form;
		
		// create db object
		$users = new Application_Model_Members();
		
		//handle post request with db object
		if($this->getRequest()->isPost()){
            if($form->isValid($_POST)){
                $data = $form->getValues();
                if($data['member_password'] != $data['confirmPassword']){
					
                    $this->view->errorMessage = "Password and confirm password don't match.";
                    return;
                }
                if($users->checkUnique($data['member_login'])){
					
                    $this->view->errorMessage = "Name already taken. Please choose      another one.";
                    return;
                }
				
                unset($data['confirmPassword']);
                $users->insert($data);
				$this->view->errorMessage = "registration successful please log in again";
                $this->_redirect('members/login');
            }
        }
		
    }

    public function editAction()
    {
        // action body
		// check if user is logged in
		$storage = new Zend_Auth_Storage_Session();
        $login = $storage->read();
        if(!$login){
            $this->_redirect('members/login');
        }
		
		//create member ORM
		$users = new Application_Model_Members();
		$form = new Application_Form_EditProfile();
        $this->view->form=$form;
		$rows = $users->find($login->member_login);
		$rowsArray = $rows->toArray();
		
		$values = array('first_name'=>$login->first_name,
						'last_name'=>$login->last_name,
						'email'=>$login->email,
						'birthday'=>(string)$rowsArray[0]["birthday"],
						);
		$form->populate($values);
		
		if($this->getRequest()->isPost()){
            if($form->isValid($_POST)){
                $data = $form->getValues();
                if($data['member_password'] != $data['confirmPassword']){
					
                    $this->view->errorMessage = "Password and confirm password don't match.";
                    return;
                }
                
				$UpdateData = array('first_name'=>$data['first_name'],
									'last_name'=>$data['last_name'],
									'email'=>$data['email'],
									'birthday'=>$data['birthday'],
									'member_password'=>$data['member_password'],
									); 
				
				$where = $users->getAdapter()->quoteInto('member_login = ?', $login->member_login);
				
                $users->update($UpdateData, $where);
				
                $this->view->errorMessage = 'Update successful';
            }
        }
    }


}









