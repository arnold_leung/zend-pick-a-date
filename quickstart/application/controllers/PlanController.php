<?php

class PlanController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {	
		$storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
		
		if (isset($data)){
			$this->view->login_id = $data->member_login;
		}else{
			$this->view->login_id =null;
		}
		
        
		$plan = new Application_Model_PlansMapper();
        $this->view->entries = $plan->fetchAll();
    }

    public function createAction()
    {
	//check if user has logged in
		$storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if(!isset($data)){
            $this->_redirect('/members');
			return;
        }
	//request handler
		$request = $this->getRequest();
        //load new plan form
		$form    = new Application_Form_Newplan();
		
		//if request is a post request (submit button)
		if ($this->getRequest()->isPost()) {

			if(isset($_POST['submit'])){
			//check if inputs are valid
				if ($form->isValid($request->getPost())) {
					//get value from form and create plan object
				
					$plans = new Application_Model_Plans($form->getValues());
					$plans->author = $data->member_login;
					// create mapper object
					$mapper  = new Application_Model_PlansMapper();
					//save new entry to DB with Mapper object
					$mapper->save($plans);
					return $this->_helper->redirector('index');
				}
			}
        }
		
		$this->view->form = $form;
    }

    public function detailAction()
    {
        // action body
		
		//check if user has logged in
		$storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if(!isset($data)){
            $this->_redirect('/members');
			return;
        }
		
		//request handling with ID show details
		$request = $this->getRequest();
		$id = (int)$request->getParam("id");
        
        $plan    = new Application_Model_Plans();
		$mapper  = new Application_Model_PlansMapper();
		
		$mapper->find($id, $plan);
		
		$this->view->plan = $plan;
		
		
		//pass comment entries to view
		
		$PlanComment = new Application_Model_CommentMapper();
       
		$this->view->PlanComment = $PlanComment->fetchByPlanId($id);
		
		//comment submit form
		$form = new Application_Form_Comment();
		$this->view->form = $form;
		
		//if request is a post request (submit button)
		if ($this->getRequest()->isPost()) {
			//check if inputs are valid
            if ($form->isValid($request->getPost())) {
				//get value from form and create plan object
				
                $comment = new Application_Model_Comment($form->getValues());
				$comment->plan_id = $id;
				
				// create mapper object
                $mapper  = new Application_Model_CommentMapper();
                //save new entry to DB with Mapper object
				$mapper->save($comment);
				$form->reset();
				$params = array('id' => $id);
				return $this->_helper->redirector('detail', 'plan','', $params);
               
            }
        }
		

    }

    public function editAction()
    {
		
        // action body
		//check if user is logged in
		$storage = new Zend_Auth_Storage_Session();
        $login = $storage->read();
        if(!$login){
            $this->_redirect('member/login');
        }
		//get plan id to be edit
		$request = $this->getRequest();
		$plan_id = (int)$request->getParam("id");
		
		//set up form
		$plan = new Application_Model_Plans();
		$mapper = new Application_Model_PlansMapper();
		$form = new Application_Form_EditPlan();
		//pass form to view
		$this->view->form=$form;
		//get plan object
		$mapper->find($plan_id, $plan);
		
		// generate value array
		$values = array('name'=> $plan->name,
						'venue'=> $plan->venue,
						'activity'=> $plan->activity,
						'gift'=> $plan->gift,
						'note'=> $plan->note,
						);
		//populate form with array
		$form->populate($values);
		
		
		if ($this->getRequest()->isPost()) {
		
			if(isset($_POST['submit'])){
			//check if inputs are valid
				if ($form->isValid($request->getPost())) {
					//get value from form and create plan object
				
					$plans = new Application_Model_Plans($form->getValues());
					$plans->author = $login->member_login;
					$plans->id = $plan_id;
					// create mapper object
					$mapper  = new Application_Model_PlansMapper();
					//save new entry to DB with Mapper object
					$mapper->save($plans);
					
				}
			}
        }
	
    }

    public function searchAction()
    {	

        // action body
		$request = $this->getRequest();
		$keyword = $request->getParam('name');
		
		$storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
		if (isset($data)){
			$this->view->login_id = $data->member_login;
		}else{
			$this->view->login_id =null;
		}
		
		if (!null ==$keyword){
		$plan = new Application_Model_PlansMapper();
		
		$table = $plan->getDbTable();
		$select = $table->select();
		$select->where('name LIKE ?', '%'.$keyword.'%');
		$rows = $table->fetchAll($select);
		
		$this->view->message = 'Search by Name Plan with "'.$keyword.'"';
        $this->view->entries = $rows;
		}
	}
    


}









