-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2015 �?04 ??10 ??23:05
-- 伺服器版本: 5.6.21
-- PHP 版本： 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫： `test`
--

-- --------------------------------------------------------

--
-- 資料表結構 `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
`id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `author` text NOT NULL,
  `comment` text NOT NULL,
  `time_created` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `guestbook`
--

CREATE TABLE IF NOT EXISTS `guestbook` (
`id` int(11) NOT NULL,
  `email` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT 'noemail@test.com',
  `comment` text CHARACTER SET latin1,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- 資料表的匯出資料 `guestbook`
--

INSERT INTO `guestbook` (`id`, `email`, `comment`, `created`) VALUES
(1, 'ralph.schindler@zend.com', 'Hello! Hope you enjoy this sample zf application!', '2015-04-07 18:19:38'),
(2, 'foo@bar.com', 'Baz baz baz, baz baz Baz baz baz - baz baz baz.', '2015-04-07 18:19:38'),
(3, 'arnoldleung26@hotmail.com', 'tttttt', '2015-04-07 14:25:22'),
(4, 'arnold@abbc.com', 'arnold', '2015-04-07 20:11:03'),
(5, 'aa@bb.com', 'qweqw', '2015-04-07 20:13:16');

-- --------------------------------------------------------

--
-- 資料表結構 `members`
--

CREATE TABLE IF NOT EXISTS `members` (
`member_id` int(11) NOT NULL,
  `member_login` varchar(20) NOT NULL,
  `member_password` varchar(20) NOT NULL,
  `member_level` int(11) NOT NULL DEFAULT '1',
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `notes` text
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `members`
--

INSERT INTO `members` (`member_id`, `member_login`, `member_password`, `member_level`, `first_name`, `last_name`, `email`, `phone`, `birthday`, `address`, `notes`) VALUES
(1, 'admin', 'admin', 2, '1234', '1234', '123@abc.com', NULL, '2015-03-18', NULL, NULL),
(2, 'guest', 'guest', 1, 'Guest', 'Account', 'guest@localhost', NULL, '1980-10-21', NULL, NULL),
(6, 'steven', '1234', 1, 'steven', 'steven', 'cibsteven@gmail.com', 'steven', '2015-03-13', 'steven', NULL),
(21, 'aaa', 'aaa', 1, 'aaa', 'aaa', 'aaa@gmail.com', 'aaa', '2001-10-10', 'aaa', NULL),
(22, 'arnold', '', 1, '', '', '', NULL, NULL, NULL, NULL),
(23, 'arnold2', '12345', 1, 'arnold', 'leung', 'arnold.leung@hotmail.com', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `plans`
--

CREATE TABLE IF NOT EXISTS `plans` (
`id` int(11) NOT NULL,
  `venue` text NOT NULL,
  `gift` text NOT NULL,
  `activity` text NOT NULL,
  `note` text NOT NULL,
  `name` text NOT NULL,
  `author` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `plans`
--

INSERT INTO `plans` (`id`, `venue`, `gift`, `activity`, `note`, `name`, `author`) VALUES
(8, 'hong kong', 'edit gift', 'aaa', 'asdfadsfdafda', 'hello', 'admin'),
(9, 'this is a test', 'edit gift', 'aaa', 'asdfadsfdafda', 'hello', 'admin');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `guestbook`
--
ALTER TABLE `guestbook`
 ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`member_id`);

--
-- 資料表索引 `plans`
--
ALTER TABLE `plans`
 ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `comment`
--
ALTER TABLE `comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- 使用資料表 AUTO_INCREMENT `guestbook`
--
ALTER TABLE `guestbook`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- 使用資料表 AUTO_INCREMENT `members`
--
ALTER TABLE `members`
MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- 使用資料表 AUTO_INCREMENT `plans`
--
ALTER TABLE `plans`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
